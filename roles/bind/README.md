# BIND

This role creates an LXC container that hosts a
[BIND](https://www.isc.org/downloads/bind/) DNS server.

## Variables

  - `bind_container_name` - the name of the container. Default `dns01`. The role
  assigns this to `container_name`, which it then uses internally.
  - `bind_ssh_port` - the ssh port to use

## Tasks

Everything is in [tasks/main.yml](task/main.yml)

## Dependencies

  - [container](../container) - explicit dependency

## Files

  - [db.51.15.78.j2](files/db.51.15.78.j2) - Reverse lookup entries for
  svinta.pl
  - [db.svinta.pl.j2](files/db.svinta.pl.j2) - DNS entries for svinta.pl
  - [named.conf.local.j2](files/named.conf.local.j2) - local BIND configuration
  for svinta.pl
  - [named.conf.options.j2](files/named.conf.options.j2) - BIND configuration options
