# Wordpress

This role creates an LXC container that hosts an
[Wordpress](https://https://wordpress.org//) server. Wordpress is a website
content management system.

## Variables

  - `wordpress_container_name` - the name of the container. Default `wordpress01`.
    The role assigns this to `container_name`, which it then uses internally.
  - `wordpress_ssh_port` - the ssh port to use
  - `wordpress_url` - the URL that is used to access Wordpress. It will generate a
    LetsEncrypt certificate for it.
  - `mysql_container_name` - the name of the MySQL container. Default `mysql01`.
  - `web_proxy_container` - the name of the web proxy container. Default `web01`.
  - `letsencrypt_email` - the email that LetsEncrypt will notify when
    certificates near expiration.
  - `wordpress_mysql_db_user` - the user with permissions to access the MySQL
    database for Wordpress.
  - `wordpress_mysql_db_password` - the password for the MySQL user.
  - `wordpress_mysql_db_name` - the name of Wordpress' MySQL database.

## Tasks

Everything is in [tasks/main.yml](tasks/main.yml).

## Dependencies

  - [web-proxy](../web-proxy) - implicit dependency and thus needs to be included in the
  playbook. This was done to prevent the container role that web-proxy depends
  on being run multiple times.
  - [mysql](../mysql) - explicit dependency
  - [container](../container) - explicit dependency

## Files

  - [proxy.conf.j2](files/proxy.conf.j2) - Nginx proxy configuration for
    proxying traffic to the Wordpress container.

