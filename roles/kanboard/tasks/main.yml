---
- name: get container details
  lxc_container:
      name: "{{ container_name }}"
      state: started
  register: container_details
- name: get mysql container details
  lxc_container:
      name: "{{ mysql_container }}"
      state: started
  register: mysql_container_details
- name: change ssh port to container
  set_fact:
      ansible_port: "{{ kanboard_ssh_port }}"
- name: install dependencies
  apt:
      name: [nginx, php7.0-fpm, php7.0-gd, php7.0-mysql, php7.0-mbstring, php7.0-xml, php7.0-zip, unzip]
      state: present
- name: find existing install
  find:
      paths: /var/www/html/{{ kanboard_url }}/
      pattern: index.php
  register: files_found
- name: download KanBoard
  get_url:
      url: https://kanboard.net/kanboard-latest.zip
      dest: /tmp/kanboard.zip
      mode: 0664
  when: files_found.matched == 0
- name: unarchive KanBoard
  unarchive:
      copy: no
      src: /tmp/kanboard.zip
      dest: /tmp/
  when: files_found.matched == 0
- name: install KanBoard
  shell: mv /tmp/kanboard/ /var/www/html/{{ kanboard_url }}
  when: files_found.matched == 0
- name: create plugin dir
  file:
      name: /var/www/html/{{ kanboard_url }}/plugins
      state: directory
      owner: www-data
      group: www-data
- name: copy config
  template:
      src: "{{ role_path}}/files/config.php.j2"
      dest: /var/www/html/{{ kanboard_url }}/config.php
      owner: root
      group: root
      mode: 0644
- name: copy site config
  template:
      src: "{{ role_path}}/files/site.conf.j2"
      dest: /etc/nginx/sites-available/{{ kanboard_url }}.conf
      owner: root
      group: root
      mode: 0644
  register: site
- name: enable site
  file:
      src: /etc/nginx/sites-available/{{ kanboard_url }}.conf
      dest: /etc/nginx/sites-enabled/{{ kanboard_url }}.conf
      state: link
      owner: root
      group: root
- name: restart nginx
  service: name=nginx state=restarted
  when: site is changed
- name: change ownership
  file:
      path: /var/www/html/{{ kanboard_url }}
      owner: www-data
      group: www-data
      recurse: yes
      state: directory
- name: enable daily corn job
  cron:
      name: "kanboard background jobs"
      hour: 8
      user: www-data
      job: "cd /var/www/html/{{ kanboard_url }} && ./cli cronjob >/dev/null 2>&1"
- name: change ssh port to mysql container
  set_fact:
      ansible_port: "{{ mysql_ssh_port }}"
- name: create database
  mysql_db:
      name: "{{ kanboard_mysql_db_name }}"
      state: present
      login_user: "{{ mysql_admin_user }}"
      login_password: "{{ mysql_admin_password }}"
      login_host: localhost
- name: create mysql user
  mysql_user:
      name: "{{ kanboard_mysql_db_user }}"
      host: "{{ container_details.lxc_container.ips[0] }}"
      password: "{{ kanboard_mysql_db_password }}"
      priv: "{{ kanboard_mysql_db_name }}.*:ALL"
      login_user: "{{ mysql_admin_user }}"
      login_password: "{{ mysql_admin_password }}"
      login_host: localhost
- name: change ssh port back to default
  set_fact:
      ansible_port: 22
- name: find proxy container
  lxc_container:
      name: "{{ web_proxy_container }}"
  register: proxy_container_details
- name: change ssh port to proxy container
  set_fact:
      ansible_port: "{{ web_proxy_ssh_port }}"
- name: copy website proxy config
  template:
      src: "{{ role_path }}/files/proxy.conf.j2"
      dest: /etc/nginx/sites-available/{{ kanboard_url }}.conf
      owner: root
      group: root
  register: proxy
- name: enable proxy site
  file:
      src: /etc/nginx/sites-available/{{ kanboard_url }}.conf
      dest: /etc/nginx/sites-enabled/{{ kanboard_url }}.conf
      owner: root
      group: root
      state: link
- name: install certbot
  apt:
      name: certbot
      state: present
- name: find current cert
  find:
      paths: /etc/letsencrypt/live
      pattern: "{{ kanboard_url }}"
      file_type: directory
  register: certbot
- name: stop nginx
  service: name=nginx state=stopped
  when: certbot.matched == 0
- name: create ssl certificate
  shell: certbot certonly -d {{ kanboard_url }} -m {{ letsencrypt_email }} -a standalone --agree-tos
  when: certbot.matched == 0
- name: start nginx
  service: name=nginx state=started
  when: certbot.matched == 0
- name: restart nginx
  service: name=nginx state=restarted
  when: proxy is changed
- name: change ssh port back to default
  set_fact:
      ansible_port: 22
