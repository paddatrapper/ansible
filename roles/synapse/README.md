# Synapse

This role creates an LXC container that hosts an
[Matrix Synapse](https://matrix.org/) server.

## Variables

  - `synapse_container_name` - the name of the container. Default `synapse01`.
  The role assigns this to `container_name`, which it then uses internally.
  - `synapse_ssh_port` - the ssh port to use
  - `synapse_url` - the URL that is used to access Synapse. It will generate a
  LetsEncrypt certificate for it.
  - `web_proxy_container` - the name of the web proxy container. Default `web01`
  - `letsencrypt_email` - the email that LetsEncrypt will notify when
  certificates near expiration.

## Tasks

Everything is in [tasks/main.yml](tasks/main.yml).

## Dependencies

  - [web-proxy](../web-proxy) - inplicit dependency and thus needs to be included in the
  playbook. This was done to prevent the container role that web-proxy depends
  on being run multiple times.
  - [container](../container) - explicit dependency

## Files
