# MeshCentral

This role creates an LXC container that hosts an
[MeshCentral](https://www.meshcommander.com/meshcentral2) server.
MeshCentral is a Node app.

## Variables

  - `meshcentral_container_name` - the name of the container. Default
  `meshcentral01`.
  The role assigns this to `container_name`, which it then uses internally.
  - `meshcentral_ssh_port` - the ssh port to use
  - `meshcentral_url` - the URL that is used to access MeshCentral. It
  will generate a LetsEncrypt certificate for it.
  - `web_proxy_container` - the name of the web proxy container. Default `web01`
  - `letsencrypt_email` - the email that LetsEncrypt will notify when
  certificates near expiration.

## Tasks

Everything is in [tasks/main.yml](tasks/main.yml).

## Dependencies

  - [web-proxy](../web-proxy) - inplicit dependency and thus needs to be included in the
  playbook. This was done to prevent the container role that web-proxy depends
  on being run multiple times.
  - [container](../container) - explicit dependency

## Files

  - [meshcentral.service.j2](files/meshcentral.service.j2) - Systemd unit for
  running MeshCentral
  - [proxy.conf.j2](files/proxy.conf.j2) - Nginx proxy configuration for
  proxying traffic to the MeshCentral container.

