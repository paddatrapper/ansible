# Blog

This role creates an LXC container that hosts a
[Moodle](https://moodle.org/) LMS server.

## Variables

  - `moodle_container_name` - the name of the container. Default `moodle01`.
  The role assigns this to `container_name`, which it then uses internally.
  - `moodle_ssh_port` - the ssh port to use
  - `moodle_url` - the URL that is used to access the moodle. It will generate a
  LetsEncrypt certificate for it.
  - `web_proxy_container` - the name of the web proxy container. Default `web01`
  - `letsencrypt_email` - the email that LetsEncrypt will notify when
  certificates near expiration.
  - `moodle_mysql_db_user` - the user with permissions to access the MySQL
    database for Mattermost.
  - `moodle_mysql_db_password` - the password for the MySQL user.
  - `moodle_mysql_db_name` - the name of Mattermost's MySQL database.

## Tasks

Everything is in [tasks/main.yml](tasks/main.yml).

## Dependencies

  - [web-proxy](../web-proxy) - inplicit dependency and thus needs to be included in the
  playbook. This was done to prevent the container role that web-proxy depends
  on being run multiple times.
  - [container](../container) - explicit dependency

## Files

  - [proxy.conf.j2](files/proxy.conf.j2) - Nginx proxy configuration for
  proxying traffic to the Airsonic container.
  - [site.conf.j2](files/site.conf.j2) - Nginx site configuration for serving
  the moodle.
