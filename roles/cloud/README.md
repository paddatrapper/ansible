# Cloud

This role ensures various `iptables` rules needed for the cloud server
(`ams1.paddatrapper.com`) are set up

## Variables

There are no variables.

## Tasks

Everything is in [tasks/main.yml](tasks/main.yml).

## Dependencies

There are no dependencies.
