# Mattermost

This role creates an LXC container that hosts an
[Mattermost](https://about.mattermost.com/) server. Mattermost is a team
group-chat application.

## Variables

  - `mattermost_container_name` - the name of the container. Default `mattermost01`.
    The role assigns this to `container_name`, which it then uses internally.
  - `mattermost_ssh_port` - the ssh port to use
  - `mattermost_url` - the URL that is used to access Mattermost. It will generate a
    LetsEncrypt certificate for it.
  - `mysql_container_name` - the name of the MySQL container. Default `mysql01`.
  - `web_proxy_container` - the name of the web proxy container. Default `web01`.
  - `letsencrypt_email` - the email that LetsEncrypt will notify when
    certificates near expiration.
  - `mattermost_mysql_db_user` - the user with permissions to access the MySQL
    database for Mattermost.
  - `mattermost_mysql_db_password` - the password for the MySQL user.
  - `mattermost_mysql_db_name` - the name of Mattermost's MySQL database.
  - `mattermost_rest_key` - the secret key for the REST API.
  - `mattermost_link_salt` - the salt for public links.
  - `mattermost_mail_username` - the username for the SMTP server that is used
    for email notifications.
  - `mattermost_mail_password` - the password for the SMTP server
  - `mattermost_mail_host` - the SMTP host to use
  - `mattermost_mail_port` - the SMTP port
  - `mattermost_invite_salt` - the salt to use for invite links

## Tasks

Everything is in [tasks/main.yml](tasks/main.yml).

## Dependencies

  - [web-proxy](../web-proxy) - implicit dependency and thus needs to be included in the
  playbook. This was done to prevent the container role that web-proxy depends
  on being run multiple times.
  - [mysql](../mysql) - explicit dependency
  - [container](../container) - explicit dependency

## Files

  - [config.json.j2](files/config.json.j2) - Mattermost configuration file
  - [mattermost.service](files/mattermost.service) - Systemd unit file for
    running Mattermost as a daemon.
  - [proxy.conf.j2](files/proxy.conf.j2) - Nginx proxy configuration for
    proxying traffic to the Mattermost container.

