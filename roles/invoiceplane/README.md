# InvoicePlane

This role creates an LXC container that hosts an
[InvoicePlane](https://invoiceplane.com/) server. InvoicePlane is a PHP
web-application that helps manage business quotatation and invoicing

## Variables

  - `invoiceplane_container_name` - the name of the container. Default `invoiceplane01`.
  The role assigns this to `container_name`, which it then uses internally.
  - `invoiceplane_ssh_port` - the ssh port to use
  - `invoiceplane_url` - the URL that is used to access Airsonic. It will generate a
  LetsEncrypt certificate for it.
  - `web_proxy_container` - the name of the web proxy container. Default `web01`
  - `letsencrypt_email` - the email that LetsEncrypt will notify when
  certificates near expiration.
  - `invoiceplane_mysql_db_name` - the MySQL database name
  - `invoiceplane_mysql_db_user` - the MySQL database user
  - `invoiceplane_mysql_db_password` - the MySQL database password
  - `mysql_admin_user` - the MySQL administrator user with permission to create
  the database
  - `mysql_admin_password` - the MySQL administrator password

## Tasks

Everything is in [tasks/main.yml](tasks/main.yml).

## Dependencies

  - [web-proxy](../web-proxy) - implicit dependency and thus needs to be included in the
  playbook. This was done to prevent the container role that web-proxy depends
  on being run multiple times.
  - [container](../container) - explicit dependency
  - [mysql](../mysql) - explicit dependency

## Files

  - [site.conf.j2](files/site.conf.j2) - Nginx site configuration for
  the InvoicePlane container.
  - [proxy.conf.j2](files/proxy.conf.j2) - Nginx proxy configuration for
  proxying traffic to the InvoicePlane container.

