# Common Meta

This role sets up the machine to be able to host services on bare metal without
containerisation. It also sets up the administrator's user account.

## Variables

  - `ci_ssh_key` - the SSH public key for the CI system

## Tasks

Everything is in [tasks/main.yml](task/main.yml)

## Dependencies

There are no dependencies

## Files

  - [20auto-upgrades](files/20auto-upgrades) - enables automatic package
  upgrades.
  - [50unattended-upgrades](files/50unattended-upgrades) - enables unattended
  upgrades of software for packages in Debian stretch repos.
  - [iptables.common.rules.j2](files/iptables.common.rules.j2) - initialises
  `iptables` to drop all traffic by default.
  - [iptables.j2](files/iptables.j2) - restores `iptables` rules. This is
  used to ensure that rules are persistent over restarts.
  - `~/.ssh/id_rsa.pub` is used implicitly and is installed as the SSH key for
  the administrator.

